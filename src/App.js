import React from 'react';
import {Example} from "./components/Example";
import {Route, Switch} from "react-router-dom";
import {NotFound} from "./components/NotFound";

function App() {
  return (
      <div>
        <Switch>
          <Route exact path="/">
            <Example />
          </Route>
          <Route path="*">
            <NotFound />
          </Route>
        </Switch>
      </div>

  );
}

export default App;
